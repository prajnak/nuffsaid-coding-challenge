import csv
import itertools
import time
import math
import re
from collections import Counter

def read_data():
  data = []
  with open('sl051bai.csv', encoding='latin-1') as f:
    csvr = csv.DictReader(f)
    for idx, row in enumerate(csvr):
      if idx == 0:
        print(f'Column names are {", ".join(row)}')
      else:
        data.append(row)
  
  cols = ["SCHNAM05", "LSTATE05", "LCITY05"]

  return [{k:v for k, v in i.items() if k in cols} for i in data]

data = read_data()

def cleanup_string(string):
  special_chars = ["._\"!+:;?@'"]
  for i in special_chars:
    string = string.replace(i, " ")
  return string

def constructDocFreq():
  global data
  doc_freqs = {}
  doc_freqs_city = {}
  for idx, doc in enumerate(data):
    text = cleanup_string(doc["SCHNAM05"])
    city = cleanup_string(doc["LCITY05"])
    state = cleanup_string(doc["LSTATE05"])
  
    terms = text.lower().split()

    for word in terms:
      try:
        doc_freqs[word].add(idx)
      except:
        doc_freqs[word] = {idx}
    
    city_terms = city.lower().split()
    for word in city_terms:
      try:
        doc_freqs_city[word].add(idx)
      except:
        doc_freqs_city[word] = {idx}
  
  for i in doc_freqs:
    doc_freqs[i] = len(doc_freqs[i])
  for i in doc_freqs_city:
    doc_freqs_city[i] = len(doc_freqs_city[i])

  return (doc_freqs, doc_freqs_city)

doc_freqs, doc_freqs_city = constructDocFreq()

def get_doc_freq(word):
  global doc_freqs
  c = 0
  if word in doc_freqs:
    return doc_freqs[word]
  else:
    return c

def get_doc_freq_city(word):
  global doc_freqs_city
  c = 0
  if word in doc_freqs_city:
    return doc_freqs_city[word]
  else:
    return c

def constructTfIDF():
  global doc_freqs, doc_freqs_city, data
  tf_idf = {}
  tf_idf_city = {}
  for idx, doc in enumerate(data):
    text = cleanup_string(doc["SCHNAM05"])
    city = cleanup_string(doc["LCITY05"])
    state = cleanup_string(doc["LSTATE05"])

    terms = text.lower().split()
    counter = Counter(terms)
    words_count = len(terms)
    for term in set(terms):
      tf = counter[term]/words_count
      df = get_doc_freq(term)
      idf = math.log((len(data) + 1) / (df + 1))

      if term not in tf_idf:
        tf_idf[term] = {}
      tf_idf[term][idx] = tf*idf

    terms2 = city.lower().split()
    counter2 = Counter(terms2)
    words_count2 = len(terms2)
    for term in set(terms2):
      tf = counter[term]/words_count2
      df = get_doc_freq_city(term)
      idf = math.log((len(data) + 1) / (df + 1))

      if term not in tf_idf_city:
        tf_idf_city[term] = {}
      tf_idf_city[term][idx] = tf*idf

  return (tf_idf, tf_idf_city)

tf_idf, tf_idf_city = constructTfIDF()

def text_to_vector(text):
  word_rgx = re.compile(r"\w+")
  words = word_rgx.findall(text)
  return Counter(words)

def get_cosine(text1, text2):
  vec1 = text_to_vector(text1)
  vec2 = text_to_vector(text2)
  intersection = set(vec1.keys()) & set(vec2.keys())
  numerator = sum([vec1[x] * vec2[x] for x in intersection])

  sum1 = sum([vec1[x] ** 2 for x in list(vec1.keys())])
  sum2 = sum([vec2[x] ** 2 for x in list(vec2.keys())])
  denominator = math.sqrt(sum1) * math.sqrt(sum2)

  if not denominator:
      return 0.0
  else:
      return float(numerator) / denominator

def rank_results(results, search_term):
  global data
  ranked_results = {}
  for docidx, score in results.items():
    doc = data[docidx]
    full_text = f'{doc["SCHNAM05"].lower()} {doc["LCITY05"].lower()}'

    cosine_score = get_cosine(search_term, full_text)
    ranked_results[docidx] = cosine_score
  
  return sorted(ranked_results.items(), key=lambda x: x[1], reverse=True)


def search_schools(search_term, num_results=3, enable_cosine=False):
  global tf_idf, data, tf_idf_city
  start_time = time.time()
  query = cleanup_string(search_term)
  tokens = query.lower().split()

  query_weights = {}
  
  for token in tokens:
    if token in tf_idf:
      for idx, score in tf_idf[token].items():
        weighted_score = score
        if idx in query_weights:
          query_weights[idx] += weighted_score
        else:
          query_weights[idx] = weighted_score

    elif token in tf_idf_city:
      for idx, score in tf_idf_city[token].items():
        weighted_score = score
        if idx in query_weights:
          query_weights[idx] += weighted_score
        else:
          query_weights[idx] = weighted_score

  l = []
  if len(query_weights) > 0:
    if enable_cosine:
      qqw = rank_results(query_weights, query)    
    else:
      qqw = sorted(query_weights.items(), key=lambda x: x[1], reverse=True)
    for i in qqw[:num_results]:
      l.append(i[0])
  end_time = time.time()
  print(f"Results for \"{search_term}\" (search took: {round(end_time - start_time, 3)}s")
  for idx, entry in enumerate(l):
    print(f'{idx+1}. {data[entry]["SCHNAM05"]}')
    print(f'{data[entry]["LCITY05"]}, {data[entry]["LSTATE05"]}')


def run_tests():
  search_terms = [
    "elementary school highland park",
    "jefferson belleville",
    "riverside school 44",
    "granada charter school",
    "foley high alabama",
    "KUSKOKWIM"
  ]

  [search_schools(query) for query in search_terms]

if __name__ == "__main__":
  run_tests()