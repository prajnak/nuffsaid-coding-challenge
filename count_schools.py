import csv
import itertools

def read_data():
  data = []
  with open('sl051bai.csv', encoding='latin-1') as f:
    csvr = csv.DictReader(f)
    for idx, row in enumerate(csvr):
      if idx == 0:
          print(f'Column names are {", ".join(row)}')
      else:
          data.append(row)
  return data

data = read_data()

def print_counts():
  global data
  schools = []
  states = []
  locales = []
  for row in data:
    schools.append(row["SCHNAM05"])
  
  data_sorted_by_state = sorted(data, key=lambda x: x['LSTATE05'])

  data_sorted_by_mlocale = sorted(data, key=lambda x: x['MLOCALE'])

  data_sorted_by_city = sorted(data, key=lambda x: x['LCITY05'])

  num_schools = len(set(schools))
  print(f"Total Schools:  {num_schools}")

  print("Schools by State:")
  [print(f"{k}: {len(list(groups))}") for k, groups in itertools.groupby(data_sorted_by_state, lambda x: x['LSTATE05'])]

  print("Schools by Metro-centric locale:")
  [print(f"{k}: {len(list(groups))}") for k, groups in itertools.groupby(data_sorted_by_mlocale, lambda x: x['MLOCALE'])]

  city_counts = []
  cities_atleast_one_school= 0
  for city, groups in itertools.groupby(data_sorted_by_city, lambda x: x['LCITY05']):
    num_schools = len(list(groups))
    city_counts.append(dict(city=city, schools=num_schools))
    if num_schools > 0:
      cities_atleast_one_school += 1

  winner = max(city_counts, key=lambda x: x['schools'])
  
  print(f'City with most schools: {winner["city"]} ({winner["schools"]} schools)')

  print(f"Unique cities with at least one school: {cities_atleast_one_school}")


if __name__ == "__main__":
    # Column names are NCESSCH, LEAID, LEANM05, SCHNAM05, LCITY05, LSTATE05, LATCOD, LONCOD, MLOCALE, ULOCALE, status05
  print_counts()